<?php require('assets/inc/header.php'); ?>


 <?php 


if(isset($_GET['c_code'])) {
  $c_code=$_GET['c_code'];

  $sepJson = file_get_contents("https://covid19.mathdro.id/api/countries/{$c_code}");
  $sepObj = json_decode($sepJson, true);
   
  $sep_confirmed_cases = $sepObj['confirmed']['value'];  //global confirmed cases
  $sep_recovered_cases = $sepObj['recovered']['value']; //global recovered cases
  $sep_deaths_cases = $sepObj['deaths']['value']; //global deaths cases
}
if(isset($_GET['c_code'])) {
  ?>
         <article class="container ">  <!-- Global Data -->
          <div class="data-section">
      <div class="data-title"><?php echo strtoupper($c_code); ?></div>
      <div class="data-grid">

        <div class="confirmed data">
          <h1><?php echo $sep_confirmed_cases; ?></h1>
          <h6>Confirmed Cases</h6>

        </div>

        <div class="deaths data">
          <h1><?php echo $sep_deaths_cases; ?></h1>
          <h6>Deaths</h6>
        </div>

        <div class="recovered data">
          <h1><?php echo $sep_recovered_cases; ?></h1>
          <h6>Recovered</h6>
        </div>

      </div>
    </div>   
       
  <?php
}
         ?>






<section id="article" class ="article">


<h2 class="text-uppercase main-heading">Coronavirus disease (COVID-19) advice for the public</h2>
<h3 class="sub-heading">Basic protective measures against the new coronavirus</h3>

<ul class="advice">

  
  
  <li  class="advice-item article-content">
    Wash your hands frequently
    
    Regularly and thoroughly clean your hands with an alcohol-based hand rub or wash them with soap and water.
    
    Why? Washing your hands with soap and water or using alcohol-based hand rub kills viruses that may be on your
    hands.
    
    
  </li>
  
  <li class="advice-item article-content">
    Maintain social distancing
    
    Maintain at least 1 metre (3 feet) distance between yourself and anyone who is coughing or sneezing.
    
    Why? When someone coughs or sneezes they spray small liquid droplets from their nose or mouth which may contain
    virus. If you are too close, you can breathe in the droplets, including the COVID-19 virus if the person
    coughing has the disease.
    
    
    
  </li>
  
  <li class="advice-item article-content">
    
    Avoid touching eyes, nose and mouth
    
    Why? Hands touch many surfaces and can pick up viruses. Once contaminated, hands can transfer the virus to your
    eyes, nose or mouth. From there, the virus can enter your body and can make you sick.
    
    
  </li>
  
  <li class="advice-item article-content">
    
    Practice respiratory hygiene
    
    Make sure you, and the people around you, follow good respiratory hygiene. This means covering your mouth and
    nose with your bent elbow or tissue when you cough or sneeze. Then dispose of the used tissue immediately.
    
    Why? Droplets spread virus. By following good respiratory hygiene you protect the people around you from viruses
    such as cold, flu and COVID-19.
    
    
  </li>
  
  <li class="advice-item article-content">
    If you have fever, cough and difficulty breathing, seek medical care early
    
    Stay home if you feel unwell. If you have a fever, cough and difficulty breathing, seek medical attention and
    call in advance. Follow the directions of your local health authority.
    
    Why? National and local authorities will have the most up to date information on the situation in your area.
  Calling in advance will allow your health care provider to quickly direct you to the right health facility. This
  will also protect you and help prevent spread of viruses and other infections.
  
</li>


<li class="advice-item article-content">
  
  6. Stay informed and follow advice given by your healthcare provider
  
  Stay informed on the latest developments about COVID-19. Follow advice given by your healthcare provider, your
  national and local public health authority or your employer on how to protect yourself and others from COVID-19.

  Why? National and local authorities will have the most up to date information on whether COVID-19 is spreading
  in your area. They are best placed to advise on what people in your area should be doing to protect themselves.
  
  
</li>

</ul>

</section>


</article>



<?php require('assets/inc/footer.php'); ?>