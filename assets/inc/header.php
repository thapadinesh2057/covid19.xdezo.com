<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Document</title>
  <script src="/assets/js/vue.js"></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

  <script src="https://kit.fontawesome.com/019078b0f5.js" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="/assets/css/app.min.css" />
</head>

<body >
  <header id="navtop">

    <nav>
      <div class="nav-slider"  v-bind:class="{ show: navopen }">

        <div class="" @click="tooglenav"  >
          <i class="fas fa-times" id="burger-menu" ></i>
        </div>

        
  

        <ul class="slider-items" >
          <li>
            <i class="fas fa-home"></i>
            <a href="/" class="slider-links">Covid Home</a>
          </li>
          <li>
            <i class="fas fa-globe-americas"></i>
            <a href="/covid-global.php" class="slider-links">Covid Global</a>
          </li>
          <li>
            <i class="fas fa-globe-americas"></i>

            <a href="/covid-guides.php" class="slider-links">Covid Guides</a>
          </li>
          <li>
            <i class="fas fa-globe-americas"></i>

            <a href="/covid-ebooks.php" class="slider-links">Covid E-Books</a>
          </li>
          <li>
            <i class="fas fa-globe-americas"></i>

            <a href="/covid-helpful.php" class="slider-links">Helpful Articles & Portals</a>
          </li>
        </ul>

        
      </div>

      

      <div class="nav-top-row">
        <div class="burger" @click="tooglenav"  >
          <i class="fas fa-bars" id="burger" ></i>
        </div>

        <div class="brand-logo-section ">
          <a href="/"><img class="brand-logo" src="/assets/img/xdezo_black.png" alt="brand logo" /></a>
        </div>
      </div>
      
      <div class="nav-bottom-row ">
        <ul class="nav-items container">
          <li>
            <i class="fas fa-home"></i>
            <a href="/" class="nav-links">Covid Home</a>
          </li>
          <li>
            <i class="fas fa-globe-americas"></i>
            <a href="/covid-global.php" class="nav-links">Covid Global</a>
          </li>
          <li>
            <i class="fas fa-globe-americas"></i>

            <a href="/covid-guides.php" class="nav-links">Covid Guides</a>
          </li>
          <li>
            <i class="fas fa-globe-americas"></i>

            <a href="/covid-ebooks.php" class="nav-links">Covid E-Books</a>
          </li>
          <li>
            <i class="fas fa-globe-americas"></i>

            <a href="/covid-helpful.php" class="nav-links">Helpful Articles & Portals</a>
          </li>
        </ul>
      </div>
    </nav>




  </header>