<?php require('assets/inc/header.php'); ?>


<article class="container ">
 
    <section id="article" class ="article">
         
    

<li class="text-uppercase main-heading">Answers To Your Coronavirus Questions</li>
<h3 class="sub-heading"> <a href="public/nyt-coronavirus-answers.pdf" target="_blank"><p style="padding-left: 20px;font-size: 20px; color:#1E9FF2;">Download</p></a>
</h3>

<li class="text-uppercase main-heading">Coronavirus Fact Sheets</li>
<h3 class="sub-heading" > <a href="public/2019-ncov-factsheet.pdf" target="_blank"><p style="padding-left: 20px;font-size: 20px;color:#1E9FF2;">Download</p></a>
</h3>

<li class="text-uppercase main-heading">Guidelines for the use of non-pharmaceutical measures to delay and mitigate the impact of 2019-nCoV</li>
<h3 class="sub-heading">              <a href="public/novel-coronavirus-guidelines-non-pharmaceutical-measures_0.pdf" target="_blank"><p style="padding-left: 20px;font-size: 20px;color:#1E9FF2;">Download</p></a>
</h3>

<li class="text-uppercase main-heading">Coronavirus: A Book For Children</li>
<h3 class="sub-heading"><a href="public/Coronavirus-A-Book-for-Children.pdf" target="_blank"><p style="padding-left: 20px;font-size: 20px;color:#1E9FF2;">Download</p></a>
</h3>




        
        
        </section>
</article>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<?php require('assets/inc/footer.php'); ?>