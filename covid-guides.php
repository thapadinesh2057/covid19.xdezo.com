<?php require('assets/inc/header.php'); ?>

<article class="container ">

<section id="article" class ="article">

<h2 class="text-uppercase main-heading">Coronavirus disease (COVID-19) Guides</h2>

<h3 class="sub-heading">Overview</h3>
              
              
        <p  class="advice-item article-content">


          Coronavirus disease (COVID-19) is an infectious disease caused by a newly discovered coronavirus. <br>
          
          Most people infected with the COVID-19 virus will experience mild to moderate respiratory illness and recover without requiring special treatment.  Older people, and those with underlying medical problems like cardiovascular disease, diabetes, chronic respiratory disease, and cancer are more likely to develop serious illness.
          <br>
          The best way to prevent and slow down transmission is be well informed about the COVID-19 virus, the disease it causes and how it spreads. Protect yourself and others from infection by washing your hands or using an alcohol based rub frequently and not touching your face. <br>
          
          The COVID-19 virus spreads primarily through droplets of saliva or discharge from the nose when an infected person coughs or sneezes, so it’s important that you also practice respiratory etiquette (for example, by coughing into a flexed elbow).
          <br>
          At this time, there are no specific vaccines or treatments for COVID-19. However, there are many ongoing clinical trials evaluating potential treatments. WHO will continue to provide updated information as soon as clinical findings become available.
             
      </p>





      <h2 class="text-uppercase main-heading">Prevention</h2>

<h3 class="sub-heading">To prevent infection and to slow transmission of COVID-19, do the following:</h3>


<li  class="advice-item article-content">
   Wash your hands regularly with soap and water, or clean them with alcohol-based hand rub. <br>
</li>

<li  class="advice-item article-content">
   Maintain at least 1 metre distance between you and people coughing or sneezing. <br>
</li>

<li  class="advice-item article-content">
  Avoid touching your face. <br>
</li>

<li  class="advice-item article-content">
   Cover your mouth and nose when coughing or sneezing. <br>
</li>
<li  class="advice-item article-content">
  Stay home if you feel unwell. <br>
</li>

<li  class="advice-item article-content">
   Refrain from smoking and other activities that weaken the lungs. <br>
</li>

<li  class="advice-item article-content">
   Practice physical distancing by avoiding unnecessary travel and staying away from large groups of people.
</li>

             



<h2 class="text-uppercase main-heading">Symptoms</h2>

<h3 class="sub-heading">The COVID-19 virus affects different people in different ways.  COVID-19 is a respiratory disease and most infected people will develop mild to moderate symptoms and recover without requiring special treatment.  People who have underlying medical conditions and those over 60 years old have a higher risk of developing severe disease and death. 
</h3>


<h3 class="sub-heading">Common symptoms include:</h3>

<li  class="advice-item article-content">
  fever 
</li>
<li  class="advice-item article-content">
tiredness 
</li>
<li  class="advice-item article-content">
cough 
</li>




<h3 class="sub-heading">Other symptoms include:</h3>
 
<li  class="advice-item article-content">
shortness of breath 
</li>
<li  class="advice-item article-content">
aches and pains 
</li>
<li  class="advice-item article-content">
sore throat
</li>
<li  class="advice-item article-content">
and very few people will report diarrhoea, nausea or a runny nose.</li>

<h3 class="sub-heading">
People with mild symptoms who are otherwise healthy should self-isolate and contact their medical provider or a COVID-19 information line for advice on testing and referral.

</h3>
 
<h3 class="sub-heading">
People with fever, cough or difficulty breathing should call their doctor and seek medical attention.

</h3>

            
        </section>
</article>

<?php require('assets/inc/footer.php'); ?>